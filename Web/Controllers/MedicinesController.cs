﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Web.Models;

namespace Web.Controllers
{
	public class MedicinesController : Controller
	{
		private IHttpClientFactory _clientFactory;

		public MedicinesController(IHttpClientFactory clientFactory)
		{
			this._clientFactory = clientFactory;
			
		}

		public IActionResult Index()
		{
			IEnumerable<Medicine> medicines = null;

			using (var client = this._clientFactory.CreateClient())
			{
				client.BaseAddress = new Uri(string.Format("{0}://{1}{2}/api/", Request.Scheme, Request.Host, Request.PathBase));
				var responseTask = client.GetAsync("medicines");
				responseTask.Wait();

				var result = responseTask.Result;
				if (result.IsSuccessStatusCode)
				{
					var readTask = result.Content.ReadAsAsync<IList<Medicine>>();
					readTask.Wait();

					medicines = readTask.Result;
				}
				else
				{
					medicines = Enumerable.Empty<Medicine>();
					ModelState.AddModelError(string.Empty, "Server error");
				}
			}

			return View(medicines);
		}

		public IActionResult Create()
		{
			return View();
		}

		[HttpPost]
		public IActionResult Create(Medicine medicine)
		{
			using (var client = this._clientFactory.CreateClient())
			{
				client.BaseAddress = new Uri(string.Format("{0}://{1}{2}/api/", Request.Scheme, Request.Host, Request.PathBase));
				var postTask = client.PostAsJsonAsync<Medicine>("medicines", medicine);
				postTask.Wait();

				var result = postTask.Result;
				if (result.IsSuccessStatusCode)
				{
					return RedirectToAction("Index");
				}
			}

			ModelState.AddModelError(string.Empty, "Server Error");
			return View(medicine);
		}

		public IActionResult Details(int id)
		{
			Medicine medicine = null;

			using (var client = this._clientFactory.CreateClient())
			{
				client.BaseAddress = new Uri(string.Format("{0}://{1}{2}/api/", Request.Scheme, Request.Host, Request.PathBase));

				var responseTask = client.GetAsync("medicines/" + id.ToString());
				responseTask.Wait();

				var result = responseTask.Result;
				if (result.IsSuccessStatusCode)
				{
					var readTask = result.Content.ReadAsAsync<Medicine>();
					readTask.Wait();

					medicine = readTask.Result;
				}
			}

			return View(medicine);
		}

		public IActionResult Edit(int id)
		{
			Medicine medicine = null;

			using (var client = this._clientFactory.CreateClient())
			{
				client.BaseAddress = new Uri(string.Format("{0}://{1}{2}/api/", Request.Scheme, Request.Host, Request.PathBase));

				var responseTask = client.GetAsync("medicines/" + id.ToString());
				responseTask.Wait();

				var result = responseTask.Result;
				if (result.IsSuccessStatusCode)
				{
					var readTask = result.Content.ReadAsAsync<Medicine>();
					readTask.Wait();

					medicine = readTask.Result;
				}
			}

			return View(medicine);
		}

		[HttpPost]
		public IActionResult Edit(int id, Medicine medicine)
		{
			using (var client = this._clientFactory.CreateClient())
			{
				client.BaseAddress = new Uri(string.Format("{0}://{1}{2}/api/", Request.Scheme, Request.Host, Request.PathBase));

				var putTask = client.PutAsJsonAsync<Medicine>("medicines/" + id.ToString(), medicine);
				putTask.Wait();

				var result = putTask.Result;
				if (result.IsSuccessStatusCode)
				{
					return RedirectToAction("Index");
				}
			}

			return View(medicine);
		}

		public IActionResult Delete(int id)
		{
			using (var client = this._clientFactory.CreateClient())
			{
				client.BaseAddress = new Uri(string.Format("{0}://{1}{2}/api/", Request.Scheme, Request.Host, Request.PathBase));

				var deleteTask = client.DeleteAsync("medicines/" + id.ToString());
				deleteTask.Wait();

				var result = deleteTask.Result;
				if (result.IsSuccessStatusCode)
				{
					return RedirectToAction("Index");
				}
			}

			return RedirectToAction("Index");
		}
	}
}