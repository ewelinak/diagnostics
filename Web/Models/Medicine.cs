﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
	public class Medicine
	{
		public int Id { get; set; }

		[Required]
		public string Name { get; set; }

		[Display(Name = "Weight per dose [mg]")]
		public double WeightPerDose { get; set; }

		[Display(Name = "Amount of doses")]
		public int DoseAmount { get; set; }

		public string Composition { get; set; }

		public string Effect { get; set; }

		public string Dosage { get; set; }

		public string Indication { get; set; }

		public string Contraindication { get; set; }

		[Display(Name = "Price [zł]")]
		public decimal Price { get; set; }
	}
}
