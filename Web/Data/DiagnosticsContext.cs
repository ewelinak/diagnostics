﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Web.Models
{
    public class DiagnosticsContext : DbContext
    {
        public DiagnosticsContext (DbContextOptions<DiagnosticsContext> options)
            : base(options)
        {
        }

        public DbSet<Web.Models.Medicine> Medicine { get; set; }
    }
}
