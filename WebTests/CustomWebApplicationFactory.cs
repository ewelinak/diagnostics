﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Web.Models;

namespace WebTests
{
	public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<Web.Startup>
	{
		protected override void ConfigureWebHost(IWebHostBuilder builder)
		{
			builder.ConfigureServices(services =>
			{
				var serviceProvider = new ServiceCollection()
					.AddEntityFrameworkInMemoryDatabase()
					.BuildServiceProvider();

				services.AddDbContext<DiagnosticsContext>(options =>
				{
					options.UseInMemoryDatabase("InMemoryAppDb");
					options.UseInternalServiceProvider(serviceProvider);
				});

				var sp = services.BuildServiceProvider();

				using (var scope = sp.CreateScope())
				{
					var scopedServices = scope.ServiceProvider;
					var context = scopedServices.GetRequiredService<DiagnosticsContext>();

					var logger = scopedServices.GetRequiredService<ILogger<CustomWebApplicationFactory<TStartup>>>();

					context.Database.EnsureCreated();

					try
					{
						SeedData.PopulateTestData(context);
					}
					catch (Exception ex)
					{
						logger.LogError(ex, "An error occurred seeding the " +
											"database with test messages. Error: {ex.Message}");
					}
				}
			});
		}
	}
}