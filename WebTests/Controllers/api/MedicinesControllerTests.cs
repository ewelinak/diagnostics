﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Web.Models;
using Xunit;

namespace WebTests.Controllers.api
{
	public class MedicinesControllerTests : IClassFixture<CustomWebApplicationFactory<Web.Startup>>
	{
		private readonly HttpClient _client;

		public MedicinesControllerTests(CustomWebApplicationFactory<Web.Startup> factory)
		{
			_client = factory.CreateClient();
		}

		[Fact]
		public async Task CanGetMedicines()
		{
			var httpResponse = await _client.GetAsync("/api/medicines");

			httpResponse.EnsureSuccessStatusCode();

			var stringResponse = await httpResponse.Content.ReadAsStringAsync();
			var medicines = JsonConvert.DeserializeObject<IEnumerable<Medicine>>(stringResponse);
			Assert.Contains(medicines, p => p.Name == "Nurofen");
			Assert.Contains(medicines, p => p.Name == "Nurofen Forte");
		}

		[Fact]
		public async Task CanGetMedicineById()
		{
			var httpResponse = await _client.GetAsync("/api/medicines/1");

			httpResponse.EnsureSuccessStatusCode();

			var stringResponse = await httpResponse.Content.ReadAsStringAsync();
			var medicine = JsonConvert.DeserializeObject<Medicine>(stringResponse);
			Assert.Equal(1, medicine.Id);
			Assert.Equal("Nurofen", medicine.Name);
		}
	}
}
