﻿using System;
using Web.Models;

namespace WebTests
{
	public static class SeedData
	{
		public static void PopulateTestData(DiagnosticsContext dbContext)
		{
			dbContext.Medicine.Add(new Medicine() { Id = 1, Name = "Nurofen", WeightPerDose = 200, DoseAmount = 12, Price = 5 });
			dbContext.Medicine.Add(new Medicine() { Id = 2, Name = "Nurofen Forte",  WeightPerDose = 400, DoseAmount = 12, Price = 7 });
			dbContext.SaveChanges();
		}
	}
}